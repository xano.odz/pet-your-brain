
function RenderTime({remainingTime}){
  
    const minutes = Math.floor(remainingTime / 60)
    const seconds = remainingTime % 60
    
    if(remainingTime === 0){
        return(
            <div className="timer">
                Tiempo terminado
            </div>
        )
    }else{
        return(
            <div className="timer">
                <div className="text">Remaining</div>
                <div className="text"><h1>{`${minutes}:${seconds}`}</h1></div>
            </div>
        )
    }
}
export default RenderTime