import React, { useState } from "react";
import { CountdownCircleTimer } from 'react-countdown-circle-timer'
import RenderTime from "./renderTimer.component";
import "../stylesSheet/clock.style.css";
import {Button} from 'react-bootstrap'
import '../stylesSheet/lengthTimeButton.style.css';

function ClockView(){ 

    const [sessionTime, setSessionTime] = useState(60)
    const [breakTime,setBreakTime] = useState(60)
    const [key, setKey] = useState(0)
    const [playClock, setPlayClock] = useState(false)
    
    //const [time, setTime] = useState(sessionTime)
    let time = sessionTime

    const letSessionTime = (sessionTime) =>{
        if(sessionTime <=60){
            sessionTime = 60
        }
        else{
            setSessionTime(sessionTime-60)
        }
    }
    
    const letBreakTime = (breakTime) =>{
        if(breakTime <= 60){
            breakTime = 60
        }
        else{
            setBreakTime(breakTime-60)
        }
    }

    const updateTime = () => {
        if(time === sessionTime){
            //setTime(breakTime)
            time = breakTime
        }else{
            time = sessionTime
            //setTime(sessionTime)
        }
    }

    updateTime()
    return(
        <div className="container">

            <div className="row">
                <div className="col">
                        <Button id="clock-buttons" onClick={()=>setPlayClock(true)}>Start</Button>{' '}
                </div>
                <div className="col">
                    <CountdownCircleTimer
                        onComplete={() => {
                        return [true, 1500] 
                        }}
                        key={key}
                        isPlaying={playClock}
                        duration={time}
                        colors={[
                            ['#31B189', 0.25],
                            ['#7ACE67', 0.25],
                            ['#FFC872', 0.25],
                            ['#FFE3B3', 0.25]
                        ]}
                    >
                        <RenderTime/>
                        
                    </CountdownCircleTimer> 
                    <br/>
                </div>
                <div className="col">
                        <Button id="clock-buttons" onClick={()=>setPlayClock(false)}>Stop</Button>{' '}
                </div>
            </div>

            <div className="row">
                <div className="col">
                    <div className="settings-section">
                        <label id="session-label">Session Length</label>
                        <div>
                            <button  id="session-decrement" onClick={()=>letSessionTime(sessionTime)} >➖</button>
                            <span id="session-length">{sessionTime/60}</span>
                            <button  id="session-increment" onClick={()=>setSessionTime(sessionTime+60)}>➕</button>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="settings-section">
                        <label id="session-label">Break Length</label>
                        <div>
                            <button  id="session-decrement" onClick={()=>letBreakTime(breakTime)} >➖</button>
                            <span id="session-length">{breakTime/60}</span>
                            <button  id="session-increment" onClick={()=>setBreakTime(breakTime+60)}>➕</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="button-wrapper">
                        <button id="clock-buttons" onClick={() => setKey(prevKey => prevKey + 1)}>
                            Restart Timer
                        </button>
                </div>
            </div> 
        </div>
    )
}
export default ClockView